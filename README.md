Задание 1
Создайте асинхронный action для получения всех постов  с помощью redux-thunk и обработайте его в редьюсере.
Создайте селектор для получения постов и используйте его в App.tsx

Задание 2
Создайте асинхронный action для добавления комментария, опишите в редьюсере логику добавления комментария в стор

Задание 3 *
Создайте асинхронный action для удаления комментария, опишите в редьюсере логику удаления комментария в сторе.

Для выполнения заданий используйте функции api и типы данных из store/Post/types