import {createCommentApi, deleteCommentAsyncApi, getPostsAsyncApi} from "../../api/Post";
import {Post, CreateComment} from './types';
import { Dispatch} from "redux";
import {AxiosResponse} from "axios";

const author = 'Purrweb';

// //////EXAMPLE///////
// export const getPosts = () => {
//   return (dispatch: Dispatch) => {
//     асинхронная функция из api после успешного завершения которой идет синхронный action
//     ).catch((error) =>{
//       обработка ошибок
//     })
//   };
// };
// //////EXAMPLE///////


///////EXAMPLE////////
// const getPostsAction = (posts: тип payload) => ({
//   type: константа типа,
//   payload: ваш payload
// });
///////EXAMPLE////////

