import {Action} from "redux";

export interface Comment {
  id: string;
  author: string;
  text: string;
}
export type CreateComment = Omit<Comment, 'id'>
 
 export interface Post {
   id: string,
   title: string,
   author: string,
   content: string,
   comments: Array<Comment>
 }

export interface IAction<T> extends Action<string> {
  type: string;
  payload: T;
  error?: boolean;
  meta?: any;
}

export interface State  {
  posts: Array<Post>,
}

