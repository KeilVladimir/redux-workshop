
import {IAction, State} from "./types";

const initialState = {
  posts: [],
  error: undefined,
}

export const PostReducer = (state:State = initialState , action:IAction<any>) => {
  switch (action.type) {
    default:
      return state
  }
}