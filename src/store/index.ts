import {applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import { PostReducer } from "./Post/reducer";

export const store = createStore(PostReducer, applyMiddleware(thunk));
