import { httpClient } from "../services/http";
import {CreateComment} from "../store/Post/types";

export const getPostsAsyncApi = async () => {
  return await httpClient.get(`/posts?_embed=comments`);
}

export const deleteCommentAsyncApi = async (id:string) => {
  return await httpClient.delete(`comments/${id}`);
}

export const createCommentApi = async (comment: CreateComment, id: string) => {
  return await httpClient.post(`/posts/${id}/comments`, comment);
}