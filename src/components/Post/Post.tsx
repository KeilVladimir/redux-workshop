import {FC, useState} from "react";
import styles from './styles.module.scss';
import { CloseIcon } from "../../assets";
import {useDispatch} from "react-redux";
import { Post as PostTypes} from "../../store/Post/types";

export  const Post: FC<PostTypes> = ({id, title, content, author, comments}) =>{
  const [comment , setComment] = useState<string>('');
  
  const onSubmit = () =>{
    setComment('');
  }
  
  
  return(
    <div className={styles.user}>
      <div className={styles.user__header}>
        <p className={styles['post__title']}>{title}</p>
      </div>
      <p className={styles['post__author']}>{author}</p>
      <p className={styles.user__info}> {content}</p>
      <h5 className={styles['comment__head-title']}>Коментарии:</h5>
      <hr/>
      <div>
      {comments?.map((com, index) =>(
          <div key={index} className={styles.comment}>
            <div>
            <p className={styles.comment__title}>{com.author}</p>
            <p className={styles.comment__body}>{com.text}</p>
            </div>
            <button className={styles['user__delete-button']}>
              <CloseIcon />
            </button>
          </div>
      ))}
      </div>
      <div className={styles['submit-box']}>
        <textarea className={styles.textarea} value={comment}  onChange={(value)=>setComment(value.currentTarget.value)}/>
        <button className={styles.button} onClick={onSubmit} type={'submit'}>Send</button>
      </div>
    </div>
  )
}